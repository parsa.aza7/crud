<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'description',
        'image',
        'published',
        'category_id',
    ];

    public const Status = [
        1 => 'منتشر شده',
        0 => 'در صف انتشار'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
