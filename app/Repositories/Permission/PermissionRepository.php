<?php

namespace App\Repositories\Permission;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PostRepository.
 *
 * @package namespace App\Repositories;
 */
interface PermissionRepository extends RepositoryInterface
{
    //
}
