<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Spatie\Permission\Models\Role;
class RoleList extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //
        $roles = Role::all();
        return view('widgets.role_list', [
            'config' => $this->config,
            'roles' => $roles
        ]);
    }
}
