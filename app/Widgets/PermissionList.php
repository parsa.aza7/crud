<?php

namespace App\Widgets;

use Spatie\Permission\Models\Permission;
use Arrilot\Widgets\AbstractWidget;

class PermissionList extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //
        $permissions = Permission::all();
        return view('widgets.permission_list', [
            'config' => $this->config,
            'permissions' => $permissions
        ]);
    }
}
