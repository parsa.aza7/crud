<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Models\User;

class UserList extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //
        $users = User::all();
        return view('widgets.user_list', [
            'config' => $this->config,
            'users' =>$users,
        ]);
    }
}
