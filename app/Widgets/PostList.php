<?php

namespace App\Widgets;

use App\Models\Post;
use Arrilot\Widgets\AbstractWidget;

class PostList extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $posts = Post::all();
        return view('widgets.post_list', [
            'config' => $this->config,
            'posts' => $posts,
        ]);
    }
}
