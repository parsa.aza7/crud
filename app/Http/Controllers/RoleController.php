<?php

namespace App\Http\Controllers;

use App\Http\Requests\Role\CreateRoleRequest;
use App\Http\Requests\Role\UpdateRoleRequest;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function __construct()
    {
//        $this->middleware('permission:administrator');
    }

    public function index()
    {
        $roles = Role::all();
        return view('roles.index', compact('roles'))->with(['permissions' => Permission::all()]);
    }


    public function create()
    {
        return view('roles.create')->with(['permissions' => Permission::all()]);
    }

    public function store(CreateRoleRequest $request)
    {
        $role = Role::create(['name' => $request->name]);
        $role->givePermissionTo($request->permissions);
        return redirect()->route('roles.index');
    }

    public function edit(Role $role)
    {
        return view('roles.create', ['role' => $role, 'permissions' => Permission::all()]);
    }

    public function update(UpdateRoleRequest $request, Role $role)
    {
        $data = $request->only('name');
        $role->update($data);
        $role->syncPermissions($request->permissions);
        return redirect()->route('roles.index');
    }

    public function createRole()
    {
        Role::create(['name' => 'admin']);
    }

    public function assignRole()
    {
        auth()->user()->assignRole('admin');
    }

}
