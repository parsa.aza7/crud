<?php

namespace App\Http\Controllers;

use App\Http\Requests\Permission\CreatePermissionRequest;
use App\Http\Requests\Permission\UpdatePermissionRequest;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    public function __construct()
    {
//        $this->middleware('permission:administrator');
    }
    public function index()
    {
        $permissions = Permission::all();
        return view('permissions.index', compact('permissions'));
    }

    public function create()
    {
        return view('permissions.create');
    }

    public function store(CreatePermissionRequest $request)
    {
        Permission::create([
            'name'=>$request->name,
        ]);
        return redirect()->route('permissions.index');
    }

    public function edit(Permission $permission)
    {
        return view('permissions.create', ['permission' => $permission]);
    }

    public function update(UpdatePermissionRequest $request, Permission $permission)
    {
        $data = $request->only('name');
        $permission->update($data);
        return redirect()->route('permissions.index');
    }

//    public function createPermission()
//    {
//        Permission::create([
//            'name' => 'create post',
//            ]);
//
//    }
}
