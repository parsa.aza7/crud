<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use Spatie\Permission\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
//        $this->middleware('permission:administrator');
    }

    public function index()
    {
        $users = User::all();
        return view('users.index', ['users' => $users])->with(['roles' => Role::all()]);
    }

    public function create()
    {
        return view('users.create')->with(['roles' => Role::all()]);
    }

    public function store(CreateUserRequest $request)
    {
        $users = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);
        $users->assignRole($request->roles);
        return redirect()->route('users.index');
    }

    public function edit(User $user)
    {
        return view('users.create', ['user' => $user, 'roles' => Role::all()]);
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        $data = $request->only('name', 'email');
        $user->syncRoles($request->roles);
        $user->update($data);
        return redirect()->route('users.index');
    }

    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->route('users.index');

    }
}
