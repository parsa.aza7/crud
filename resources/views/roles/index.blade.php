<x-app-layout>
    <x-slot name="header">

        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('نقش ها') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
{{--                @can('create role')--}}
                    <div class="d-flex justify-content-end mb2">
                        <a class="btn btn-success" href="{{route('roles.create')}}">
                            ساخت نقش
                        </a>
                    </div>
{{--                @endcan--}}
                <div class="card card-default">
{{--                    <div class="card card-header">نقش ها</div>--}}
                    <div class="card-body">
                        {{ Widget::run('role_list') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
