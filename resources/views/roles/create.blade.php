<x-app-layout>
    <x-slot name="header">

        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('نقش ها') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="card card-default">
                    <div class="card-header header-Title">
                        {{isset($role)?'ویرایش نقش':'ساخت نقش'}}
                    </div>
                    <div class="card-body">

                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul class="list-group">
                                    @foreach($errors->all() as $error)
                                        <li class="list-group-item text-danger">
                                            {{$error}}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form action="{{isset($role)? route('roles.update', $role->id): route('roles.store')}}" method="Post">
                            @csrf
                            @if(isset($role))
                                @method('Put')
                            @endif

                            <div class="form-group">
                                <label for="name">نام</label>
                                <input type="text" class="form-control" name="name" id="name" value="{{isset($role)? $role->name: ''}}">
                            </div>
                            @foreach ($permissions as $permission)
                                <label>
                                    <input type="checkbox" name="permissions[]" value="{{$permission->id}}"
                                        {{isset($role)?
                                            in_array($permission->name, $role->getPermissionNames()->toArray()) ? "checked" :"" : ''}}>
                                        {{$permission->name}}
                                </label>
                            @endforeach
                            <div class="form-group">
                                <button class="btn btn-success">
                                    {{isset($role)?'ویرایش نقش':'ساخت نقش'}}
                                </button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
