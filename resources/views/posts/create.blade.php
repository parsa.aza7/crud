<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Posts') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="card card-default">
                    <div class="card-header header-Title">
                        {{isset($post)?'Edit Post':'Create Post'}}
                    </div>
                    <div class="card-body">
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul class="list-group">
                                    @foreach($errors->all() as $error)
                                        <li class="list-group-item text-danger">
                                            {{ $error }}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="{{isset($post)? route('posts.update',$post->id): route('posts.store')}}" method="POST" enctype="multipart/form-data" class="animated fadeInUp">
                            @csrf
                            @if(isset($post))
                                @method('PUT')
                            @endif
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input placeholder="Enter Title" type="text" id="title" class="form-control" name="title" value="{{isset($post) ? $post->title:''}}">
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea placeholder="Enter Description" name="description" id="description" rows="5" cols="5" class="form-control">{{isset($post) ? $post->description : ''}}</textarea>
                            </div>
                            @if(isset($post))
                                @can('publish post')
                                    <div class="form-group">
                                        {{ Form::label('status', 'Status')}}
                                        {{ Form::select('published',$status , old('published') ,['class'=>'form-group']) }}
                                        {{--                                        <label for="published">status</label>--}}
                                        {{--                                        @foreach($status as $statuses)--}}
                                        {{--                                            <label>{{$statuses}}<input value="{{isset($statuses)? $statuses:''}}" type="checkbox" class="form-group"></label>--}}
                                        {{--                                        @endforeach--}}
                                    </div>
                                @endcan
                            @endif
                            <div class="form-group">
                                <label for="image">Image</label>
                                <input type="file" id="image" class="form-control" name="image" value="">
                            </div>
                            @if(isset($post))
                                <div class="form-group">
                                    <img src="{{asset('storage/'.$post->image)}}" style="width: 10%" alt="">
                                </div>
                            @endif
                            <div class="form-group">
                                <label for="category">Category</label>
                                <select name="category_id" id="category" class="form-group">
                                    @foreach($categories as $category)
                                        @if(isset($post))
                                            <option value="{{$category->id}}" @if($category->id == $post->category_id) selected="selected" @else value="{{$category->id}}" @endif>{{$category->name}} </option>
                                        @else
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-success">
                                    {{isset($post) ? 'Edit Post':'Create Post'}}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
