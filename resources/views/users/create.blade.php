<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Users') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="card card-default">
                    <div class="card-header header-Title">
                        {{isset($user)?'edit user':'Create User'}}
                    </div>
                    <div class="card-body">
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul class="list-group">
                                    @foreach($errors->all() as $error)
                                        <li class="list-group-item text-danger">
                                            {{ $error }}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form action="{{isset($user)? route('users.update', $user->id): route('users.store')}}"
                              method="POST" enctype="multipart/form-data" class="animated fadeInUp">
                            @csrf
                            @if(isset($user))
                                @method('PUT')
                            @endif
                            <div class="form-group">
                                <label for="name" style="direction: rtl;">Name</label>
                                <input placeholder="Enter User Name" type="text" id="name" class="form-control"
                                       name="name" value="{{isset($user) ? $user->name:''}}">
                            </div>
                            <div class="form-group">
                                {{ Form::label('email','Email') }}
                                {{ Form::text('email', isset($user)? $user->email: '', array('placeholder' => 'Enter User Email','class' => 'form-control')) }}
                            </div>
                            @if(!isset($user))
                                <div class="form-group">
                                    {{ Form::label('password', 'Password') }}
                                    {{ Form::password('password', array('placeholder' => 'Enter User Password','class' => 'form-control')) }}
                                </div>
                            @endif
                            <div class="form-group">
                                @foreach ($roles as $role)
                                    <label>
                                        <input type="checkbox" name="roles[]" value="{{$role->id}}"
                                            {{isset($user)?
                                                in_array($role->name, $user->getRoleNames()->toArray()) ? "checked" :"" : ''}}>
                                            {{$role->name}}
                                    </label>
                                @endforeach
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-success">
                                    {{isset($user) ? 'Edit User':'Create User'}}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-app-layout>
