<x-app-layout>
    <x-slot name="header">

        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('دسترسی ها') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="card card-default">
                    <div class="card-header header-Title">
                        {{isset($permission)?'ویرایش دسترسی':'ساخت دسترسی'}}
                    </div>
                    <div class="card-body">

                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul class="list-group">
                                    @foreach($errors->all() as $error)
                                        <li class="list-group-item text-danger">
                                            {{$error}}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form
                            action="{{isset($permission)?route('permissions.update',$permission->id):route('permissions.store')}}"
                            method="Post">
                            @csrf
                            @if(isset($permission))
                                @method('Put')
                            @endif

                            <div class="form-group">
                                <label for="name">نام</label>
                                <input type="text" class="form-control" name="name" id="name"
                                       value="{{isset($permission)? $permission->name:''}}">
                            </div>

                            <div class="form-group">
                                <button class="btn btn-success">
                                    {{isset($permission)?'ویرایش دسترسی':'ساخت دسترسی'}}
                                </button>

                            </div>


                        </form>


                    </div>


                </div>

            </div>
        </div>
    </div>
</x-app-layout>
