<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-body">
            <table class="table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                @foreach($posts as $post)
                    <tr>
                        <td>{{$post->title}}</td>
                        <td>{{$post->category->name}}</td>
                        <td>
                            @if($post->published === 1)
                                Published
                            @else
                                Not Published
                            @endif
                        </td>
                        <td>
                            @can('edit post')
                                <a href="{{route('posts.edit', $post->id)}}"
                                   class="btn btn-info btn-sm">Edit</a>
                            @endcan
                        </td>
                        <td>
                            @can('publish post')
                                <form action="{{route('posts.destroy', [$post])}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger btn-sm ">
                                        Delete
                                    </button>
                                </form>
                            @endcan
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>
