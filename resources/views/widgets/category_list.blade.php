<table class="table">
    <thead>
    <th>Name</th>
    <th>Post Count</th>
    </thead>
    <tbody>
    @foreach($categories as $category)
        <tr>
            <td>{{ $category->name }}</td>
            <td>
                {{ $category->posts->count() }}
            </td>
            @can('edit post')
            <td>
                <a href="{{route('categories.edit',$category)}}" class="btn btn-info btn-sm">Edit</a>
            </td>
            @endcan
            <td>
                <form action="{{route('categories.destroy', [$category])}}" method="post">
                    @csrf
                    @method("DELETE")
                    <button type="submit" class="btn btn-sm btn-danger btn-info">Delete</button>
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
