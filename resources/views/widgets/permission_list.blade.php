<table class="table">
    <thead>
    <th>Permission</th>
    </thead>
    <tbody>
    @foreach($permissions as $permission)
        <tr>
            <td>{{ $permission->name }}</td>
{{--            @can('edit permission')--}}
                <td>
                    <a href="{{route('permissions.edit', $permission)}}" class="btn btn-info btn-sm">Edit</a>
                </td>
{{--            @endcan--}}
                {{--<td>--}}
                {{--<form action="{{route('permissions.destroy', [$permission])}}" method="post">--}}
                {{--@csrf--}}
                {{--@method("DELETE")--}}
                {{--<button type="submit" class="btn btn-sm btn-danger btn-info">Delete</button>--}}
                {{--</form>--}}
                {{--</td>--}}
        </tr>
    @endforeach
    </tbody>
</table>
