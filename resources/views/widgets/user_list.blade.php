<div class="col-lg-12">
    <div class="panel panel-default">
        {{--<div class="panel-heading">--}}
        {{--Latest Posts--}}
        {{--</div>--}}
        <div class="panel-body">
            <table class="table">
                <thead>
                <tr>
                    <th>نام</th>
                    <th>ایمیل</th>
                    <th>نقش</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>
                        @foreach($user->getRoleNames() as $role)
                                {{$role}}<br>
                        @endforeach
                        </td>
                        <td>
{{--                            @can('edit user')--}}
                                <a href="{{route('users.edit',$user->id)}}"
                                   class="btn btn-info btn-sm">ویرایش</a>
{{--                            @endcan--}}
                        </td>
                        <td>
                            <form action="{{route('users.destroy', [$user])}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-sm ">
                                    حذف
                                </button>
                            </form>
                        </td>
                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
