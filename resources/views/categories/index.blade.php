<x-app-layout>
    <x-slot name="header">

        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Categories') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
{{--                @can('write post')--}}
                    <div class="d-flex justify-content-end mb2">
                        <a class="btn btn-success" href="{{route('categories.create')}}">
                            Create Category
                        </a>
                    </div>
{{--                @endcan--}}
                <div class="card card-default">
                    {{--                    <div class="card card-header">دسته بندی ها</div>--}}
                    <div class="card-body">
                        {{ Widget::run('category_list') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
